# Manejo de dockers
A partir de aqui, toda la gestión de docker se realiza con el comando “docker” seguido de los distintos parámetros. En primera instancia, para tener una visión general de docker, como por ejemplo el número de imagenes disponibles en el sistema, número de contenedores, versión, almacenamiento, etc:  
<pre>docker info
Containers: 2
Images: 3
Storage Driver: devicemapper
 Pool Name: docker-202:2-2712-pool
 Pool Blocksize: 64 Kb
 Data file: /var/lib/docker/devicemapper/devicemapper/data
 Metadata file: /var/lib/docker/devicemapper/devicemapper/metadata
 Data Space Used: 712.4 Mb
 Data Space Total: 102400.0 Mb
 Metadata Space Used: 1.0 Mb
 Metadata Space Total: 2048.0 Mb
Execution Driver: native-0.2
Kernel Version: 3.10.0-123.8.1.el7.x86_64
Operating System: Red Hat Enterprise Linux Server 7.0 (Maipo)
</pre>

Buscar y descargar imágenes de contenedores Docker  
A través de la línea de comandos podemos buscar y descargar imágenes tanto de sistemas operativos (CentOS, Debian, Fedora…) como de sistemas operativos + aplicativos y software y preinstalado.  
Buscando “centos”, vemos que además de la imagen oficial del sistema operativo (primer resultado), también hay imágenes de terceros con software y aplicaciones ya instaladas:
<pre>docker search centos  
NAME             DESCRIPTION                                     STARS     OFFICIAL AUTOMATED
centos                    The official build of CentOS.                   598       [OK]       
tianon/centos             CentOS 5 and 6, created using rinse instea...   28        [OK]
steeef/graphite-centos    CentOS 6.x with Graphite and Carbon via ng...   6         [OK]
tutum/centos              Centos image with SSH access. For the root...   6         [OK]
tutum/centos-6.4          DEPRECATED. Use tutum/centos:6.4 instead. ...   5         [OK]
gluster/gluster           GlusterFS 3.5 - CentOS 6.5 Docker repo          5         [OK]
ingensi/oracle-jdk        Official Oracle JDK installed on centos.        4         [OK]
[...]
</pre>
El siguiente paso sería descargar una de estas imágenes para poder ejecutarla en un contenedor.  
Vamos a descargar las imágenes del sistema Debian, Ubuntu y Fedora:
<pre>docker pull fedora
5a7d9470be44: Downloading [==========================>] 37.16 MB/37.16 MB
798202714a7c: Downloading [==========================>] 37.14 MB/37.14 MB
df7844587b22: Downloading [==========================>] 34.48 MB/34.48 MB
92fd7bcf9115: Pulling fs layer 
405cce5cd17d: Downloading [==================>        ] 38.18 MB/45.13 MB 5s
ec3443b7b068: Downloading [==========================>] 37.16 MB/37.16 MB
</pre>

Una vez descargadas, podemos visualizar todas las imagenes que tenemos descargadas en nuestro sistema, y por ende, disponibles para ejecutar un contenedor con ellas:
<pre>docker images
REPOSITORY          TAG                 IMAGE ID            CREATED             VIRTUAL SIZE
debian              wheezy              f6fab3b798be        9 days ago          85.1 MB
debian              7                   f6fab3b798be        9 days ago          85.1 MB
debian              7.7                 f6fab3b798be        9 days ago          85.1 MB
debian              latest              f6fab3b798be        9 days ago          85.1 MB
fedora              latest              7d3f07f8de5f        6 weeks ago         374.1 MB
</pre>

Crear y acceder a un contenedor Docker  
Para crear un nuevo contenedor ejecutamos el siguiente comando:  
<pre>docker run IMAGEN COMANDO_INICIAL</pre>  

Por ejemplo, para arrancar un contenedor basado en la imagen “debian” y accediendo directamente a la shell bash:
<pre>docker run -i -t debian /bin/bash</pre>  

Además, en caso de que la imagen no esté disponible en nuestro repositorio local vemos como la busca y descarga automáticamente:
<pre>docker run -i -t fedora /bin/bash
Unable to find image 'fedora' locally
Pulling repository fedora
7d3f07f8de5f: Download complete 
511136ea3c5a: Download complete 
782cf93a8f16: Download complete
</pre>
Y en ambos casos, tras la ejecución del comando accedemos a nuestro contenedor fedora con ID “83fada315585″:  
[root@83fada315585 /]  

Si queremos crear el contenedor con un nombre personalizado lo podemos especificar con “--name”:  
<pre>docker run -name fedora01 fedora /bin/bash</pre>  
Para detener la ejecución del contenedor ejecutamos el siguiente comando:  
<pre>docker stop [container ID]
docker stop debian01
docker stop 83fada315585
</pre>
Guardar cambios realizados en los contenedores  
Si no hacemos un “commit” de los cambios que vayamos realizando en el contenedor, al pararlo perderemos toda la información y configuraciones realizadas. El modo de ejecutar el “commit” es el siguiente:  
<pre>docker commit ID_CONTENEDOR NOMBRE_IMAGEN</pre>
Eliminar un contenedor
Para eliminar un contenedor, el parámetro “rm” a docker seguido del ID de contenedor a borrar:  
<pre>
docker rm ID_IMAGEN
docker rm debian01
</pre>
Ver listado de contenedores  
Para realizar las gestiones anteriores de iniciar, parar, guardar un contenedor, etc es necesario conocer ya sea el ID del contenedor o el nombre asignado.  
La forma más rápida de visualizar estos y otros datos relacionados con los contenedores es el parámetro “ps”:
el comando “docker ps” muestra sólo los contenedores activos y “docker ps -l” los activos y parados:
<pre>docker ps -l  
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS                   PORTS   NAMES  
257e1ff40370        fedora:latest       "/bin/bash"         7 seconds ago       Exited (0) 4 seconds ago         sleepy_babbage  
d98af487177b        debian:7            "/bin/bash"         12 seconds ago      Exited (127) 3 seconds ago       debian01  
</pre>  
Configuración avanzada de red  
Con todo lo descrito anteriormente ya se puede empezar a trabajar con Docker y tener una visión de como funciona.  
A partir de aquí es interesante conocer como funciona la configuración avanzada de red en la cual podemos, entre otras cosas, especificar un bridge de conexión para el contenedor, activar la comunicación entre contenedores, configuración de IPTABLES, DNS, IP, etc.  
Estos son algunos de los parámetros disponibles a la hora de arrancar el contenedor que establecen estas configuraciones:
Configuración de bridges:  
--bridge=BRIDGE  
Especificar la IP a la que responderá el contenedor:  
--ip=IP_ADDRESS
