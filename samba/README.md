# Notas de instalacion de docker samba  

## Requisitos previos a tener en cuenta:  
1.Disponer del paquete docker para tu ditribucion y que esta lo soporte.  

<pre>yum install docker</pre>  
2.Tener el servicio en ejecucion.    
<pre>systemctl start docker.service</pre>  
3.Que los scripts que acompañan esta documentacion han de tener permisos de ejecucion.    
<pre>chmod +x build-img-dockers.sh</pre>  
4.Si no se dispone de las imagenes de los sistemas es obligatorio tener conexion a internet.   

## Creacion de la imagen de SAMBA   
Editamos las variables del archivo build-img-dockers.sh.    
Esta instalaccion consiste en crear exportar a la red una carpeta local,como si de una carpeta compartida del sistema se tratase.   

## Comandos y parametros de la instalacion  
<pre>docker build --tag $IMAGEN .</pre>  
Durante el proceso de instalacion se crea tanto en el container como en el sistema el usuario que se usara para realizar la conexion "smbuser" con pasword "pass".  
La carpeta de pondra por defecto den /var/docker-storage/samba.  
El recurso en la red se llamara shared, todas las opciones, incluso se podrian añadir nuevos recursos modificando el smb.conf.  

## Container para netbios name.  
La complejidad de netbios hace que requiera 3 puertos abiertos.      
<pre>docker create  
    --name $NETBIOS  
    --expose 137-139  
    --publish 137-139:137-139  
    --volume $LOCALDIR/samba:/var/samba  
    --volume /var/samba  
    --volume /etc/samba   
    --entrypoint  
    nmbd $IMAGEN -F    
</pre>

## Container para smbd.    
<pre>docker create  
    --name $SAMBA  
    --expose  445   
    --publish 445:445   
    --volumes-from $NETBIOS    
    --entrypoint smbd  
    $IMAGEN -F  
</pre>  

## FORMA DE USO  
<pre>docker start netbios samba</pre>
<pre>docker stop samba netbios</pre>
Montaje del recurso desde linux.  
<pre>mount -t cifs -o user=smbuser,password=pass //192.168.1.42/shared /mnt/</pre>
<pre>smbclient -L 192.168.1.42</pre>  
Para añadir informacion desde el servidor solo basta con llenar el directorio /var/docker-storage/samba con los archivos que queremos que esten disponibles desde la web.    

## Configuraciones adicionales  
Los archivos de log son gestionados por STOUT para poder ser monitorizados atraves de comando.  
<pre>docker logs samba</pre>  
Para añadir mas recursos al container hay que editar el arvhivo smb.conf.  
Nacho Cegarra Cayetano <naicho@gmail.com>