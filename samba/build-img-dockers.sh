#!/bin/bash
##################VARIABLES#########################

IMAGEN="samba-img"
NETBIOS="netbios"
SAMBA="samba"
LOCALDIR="/var/dockers-storage/"

########### CREACION DE IMAGEN BASE ###############

docker build \
	--tag $IMAGEN .

########### PREPARAMOS EL SYSTEMA #################

# Paramos y removemos los containers si existen
docker stop $NETBIOS $SAMBA &>/dev/null
docker rm $NETBIOS $SAMBA &>/dev/null


# creamos el directorio local donde tendremos un backup 
# en el caso de que exista el directorio lo borramos
rm -rf $LOCALDIR/samba &>/dev/null
mkdir -p $LOCALDIR/samba &>/dev/null


############## CREAMOS LOS CONTAINERS ##############
# NETBIOS CONTAINER


docker create \
	--name $NETBIOS \
	--expose 137-139 \
	--publish 137-139:137-139 \
	--volume $LOCALDIR/samba:/var/samba \
	--volume /var/samba \
	--volume /etc/samba \
	--entrypoint nmbd \
	$IMAGEN \
	-F

docker create \
	--name $SAMBA \
	--expose  445 \
	--publish 445:445 \
	--volumes-from $NETBIOS \
	--entrypoint smbd \
	$IMAGEN \
	-F

####################################################
#se ejecuta por este orden, y en una linea
# docker start netbios samba
# docker stop netbios samba


