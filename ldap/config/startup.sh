#!/bin/bash
# Archivo de startup que se ejecuta al crear/arrancar el docker ldapserver

##### VARIABLES ########################################################
adminDN='cn=Manager,dc=nacho,dc=org'
pass='jupiter'

########################################################################
#comprobamos si existe la base de datos , sino se crea
########################################################################

if ! [[ -e /var/lib/ldap/DB_CONFIG ]]
then 
    cp /etc/openldap/config/DB_CONFIG /var/lib/ldap/
    rm -rf /etc/openldap/slapd.d/*
    slaptest -f /etc/openldap/config/slapd.conf -F /etc/openldap/slapd.d 
    slaptest -f /etc/openldap/config/slapd.conf -F /etc/openldap/slapd.d -u
    chown -R ldap:ldap /var/lib/ldap
    chown -R ldap:ldap /etc/openldap/config
    chown -R ldap:ldap /etc/openldap/slapd.d
    slapd &&
    ldapadd -f /etc/openldap/config/estructura.ldif -x -D $adminDN -w $pass 
    ldapadd -f /etc/openldap/config/grupos.ldif -x -D $adminDN -w $pass
    ldapadd -f /etc/openldap/config/usuarios.ldif -x -D $adminDN -w $pass
fi
exec /usr/sbin/slapd -d 0
