#!/bin/bash
##################VARIABLES#########################

IMAGEN="ldap-img"
LOCALDIR="/var/dockers-storage"
LDAP="ldapserver"
REALM=".nacho.org"

########### CREACION DE IMAGEN BASE ###############

docker build \
	--tag $IMAGEN .

########### PREPARAMOS EL SISTEMA #################

# Paramos y removemos el container si existe
docker stop $LDAP &>/dev/null
docker rm $LDAP &>/dev/null

# Creamos el directorio local donde tendremos un backup
# del directorio /var/lib/ldap
rm -rf $LOCALDIR/openldap &>/dev/null
mkdir -p $LOCALDIR/openldap &>/dev/null


############## CREAMOS EL CONTAINER FINAL #########

docker create \
	--name $LDAP \
	--expose 389 \
	--expose 666 \
	--hostname "ldap.nacho.org" \
	-p 389:389 \
	-p 666:666 \
	--restart always \
	--volume /etc/localtime:/etc/localtime:ro \
	--volume $LOCALDIR/openldap:/var/lib/ldap \
	$IMAGEN /etc/openldap/config/startup.sh



# Arracamos con el siguiente comenado
# docker [start|stop] ldapserver
