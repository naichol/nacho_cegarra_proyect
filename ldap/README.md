# Notas de instalacion de ldapserver
## Requisitos previos a tener en cuenta:
1.Disponer del paquete docker para tu ditribucion y que esta lo soporte.  
<pre>yum install docker</pre>
2.Tener el servicio en ejecucion.  
<pre>systemctl start docker.service</pre>
3.Que los scripts que acompañan esta documentacion han de tener permisos
 de ejecucion. 
<pre>chmod +x build-img-dockers.sh</pre>
<pre>chmod +x startup.sh</pre>
4.Si no se dispone de las imagenes de los sistemas es obligatorio tener
conexion a internet.  
## Creacion de la imagen de LDAP  
Editamos las variables del archivo build-img-dockers.sh.
El script crea a modo "prueba" 3 grupos ,3 usuarios, 3 estructuras de
organizacion, los passwords de los usuarios son el mismo que el nombre.
Si deseamos podemos poner nuestros propios ficheros de slapd.conf ,
usuarios, grupos ,etc.  

## Comandos y parametros de la instalacion
<pre>docker build --tag $IMAGEN .</pre>  
Durante el proceso de build se copia la carpeta config a /etc/openldap/configuracion.
El "entrypoint" del container es el propio script startup.sh.  
Al arrancar por primera vez el contenedor el detecta que la base no esta
instalada,por lo tanto ejecuta las ordenes de creacion de base de datos.  
Este proceso no se volvera a repetir en futuras ejecuiones del script. 

## Container para el ldapserver.  
Le montamos la carpeta /etc/localtime como solo lectura por que el 
contenedor no tiene la misma configuracion de hora que el host,y nos puede
dar muchos problemas.  
<pre>docker create 
    --name $LDAP 
    --expose 389
    --expose 666 
    --publish 389:389 
    --publish 666:666 
    --restart always 
    --volume /etc/localtime:/etc/localtime:ro 
    --volume $LOCALDIR/openldap:/var/lib/ldap 
    --entrypoint /etc/openldap/config/startup.sh $IMAGEN
</pre>
## Forma de uso
<pre>docker start ldapserver</pre> 
<pre>docker stop ldapserver</pre>  
El contenedor esta creado con el parametro --restart always que quiere
decir que siempre que el servicio de docker este funcionando se activara
dicho container.
Puede ser muy interesante activar el docker siempre en el sistema.
<pre>systemctl enable docker.service</pre>
De esta manera se automatizan los servicios con el arranque del sistema.

## Configuraciones adicionales
Los archivos de log son gestionados por STDERR para poder ser monitorizados
atraves de comando.
<pre>docker logs ldapserver</pre>

Si quisieramos reinstalar el docker y mantener las cuentas, es tan facil
como renombrar la carpeta /var/docker-storage/openldap a openldap.bak ,
ejecutar el instalador copiar todo el contenido de la carpeta vieja dentro
de la nueva , (con los dockers apagados) y al levantarlos ya estarian listos
para hacer un start con la configuracion vieja.

Si nuestra intencion es añadir mas usuarios se deben de realizar mediante
la orden cliente ldapadd.

Pruebas desde maquinas remotas  
Adjunto una carpeta llamada archivos cliente donde hay un pequeño ldif para hacer pruebas de ldapadd, y el ldap.conf que necesitan los clientes.
<pre>ldapsearch -x -b 'ou=usuarios,dc=nacho,dc=org' -H ldap://server.nacho.org/</pre>
<pre>ldapadd -x .w $pass -D $adminDN -f usuarios.ldif </pre>
<pre>ldapadd -x -w $pass -D $adminDN -f usuarios.ldif -H ldap://$SERVERNAME/ </pre>

Esto es aplicable tambien a modificaciones, altas y bajas.

Nacho Cegarra Cayetano <naichol@gmail.com>