% Dockerizando un servidor empresarial
% Nacho Cegarra Cayetano
% Mayo 26, 2015

# De que vamos a hablar?

- Son dockers unos pantalones?  
 
- Que es un servidor empresarial?

# Cual es el objetivo de esta exposición?

- Comprender por que razon es una revolucion

- Familiarizarse con esta tecnologia

- Comprender el funcionamiento interno de los contenedores

- Cuales pueden ser sus posibles implementaciones 

# Empezamos

# Algo pasajero que usan 4 frikis...

#### Empresas que implementan dockers  
- Ebay (16.050 mill)  
- Redhat (1330 mill)  
- Baidu (651 mill) 
- Amazon (34.204 mill)  
- Oracle (14.380 mill)  
- etc
- etc
- .
- Ah por cierto tambien Microsoft apartir del 2016
 
# Pros y contras
#### Que ventajas tiene sobre una maquina virtual?  
-Portabilidad  
-Escalabilidad  
-Replicacion de las infraestructuras  
-Diversidad para los desarrolladores  
-Bajos costes  
-Segmentacion de problemas  
-No dispones de un hipervisor

#### Inconvenientes
-Aun se esta madurando el proyecto  
-Hay determinados problemas que veremos mas adelante  
-Plataforma linux y similares  
-Aprender una nueva tecnologia

# Como funciona un container?
  
*"Un kernel para dominarlos a todos..."*  

# Mas capas que una cebolla

- La aplicacion de dockers funciona mediante capas o layers
- Bien, y que ventajas tiene eso?
  Suponen un problema de almacenamiento?
  Desventajas?

# El servicio docker 
Adiós al systemctl y al systemd  
Apartir de ahora será docker el "ordeno y mando de los contenedores"

Como funcionan los procesos si no hay systemd?

#### De que partes esenciales se compone un servicio docker?

- Repositorio de imagenes GitHub
- Imagenes base
- App de docker
- Contenedores
- Capas

# Lo quiero ya!!!  
  
### Que hace falta?  
  
Una distribucion de linux que lo soporte o sistemas similares como Core OS  
o las imagenes de amazon, o el boot2docker para maquinas windows.

### Cuanto cuesta?  

Oferta especial 0€  

### Como lo descargo?     
-Desde el propio repositorio de fedora se descarga    
<pre>yum install docker-io fedora-dockerfiles docker-io-vim</pre>
-Hay disponible una gran cantidad de plugins herramientas visuales etc.  
-Documentacion en la web oficial [http://docker.com](http://www.docker.com)  

# Una vez descargado activamos el servicio
<pre>systemctl start docker.service</pre>
<pre>systemctl enable docker.service</pre>
Creamos un grupo en el systema para manejar el socket de docker.
Por ejemplo el grupo "docker" 
<pre>groupadd docker</pre> 
Asignamos a nuestro usuario del sistema el grupo docker como grupo secundario  
<pre>usermod -a -G docker nacho</pre>
Cambiamos el propietario del socket a ese nuevo grupo.  
<pre>chown nacho:docker /var/run/docker.sock</pre>  
Se pueden consultar las diferentes opciones del comando docker en el manpage.  

# De que comandos dispongo?
*"El man es nuestro amigo"*  
<pre>
man docker-*
docker          docker-history  docker-pause    docker-save
docker-attach   docker-images   docker-port     docker-search
docker-build    docker-import   docker-ps       docker-start
docker-commit   docker-info     docker-pull     docker-stats
docker-cp       docker-inspect  docker-push     docker-stop
docker-create   docker-kill     docker-rename   docker-tag
docker-diff     docker-load     docker-restart  docker-top
docker-events   docker-login    docker-rm       docker-unpause
docker-exec     docker-logout   docker-rmi      docker-version
docker-export   docker-logs     docker-run      docker-wait
</pre>

# Lo mas sencillo es ir a la practica.  

- Acontinuacion les mostrare como implementé un servidor kerberos.

- Implementacion de un servidor LDAP

- Implementacion de un servicio de carpeta compartida en SAMBA

# Pero no todo fue un paseo de rosas...

# El maldito NFSD   
Daemon de servicio compartido de ficheros por la red.  
Al implementar este servicio me encontre multitud de problemas...   

# En que mas se puede aplicar?  
- Estructuras de servidores en la nube (atomic/atom)
- Para dokerizar aplicaciones propias y aislarlas
- Para crear clusters de containers
- Para testear aplicaciones en diferentes entornos sin coste

# Conclusión 

- La tecnologia docker es facil, versatil y es el futuro inmediato.

- Es muy buena idea implementar dockers pero hay que tener cuidado de  
*"No dockerizar por encima de nuestras posibilidades"* 

- Tener encuenta que es una plataforma en desarrollo y por lo tanto no es aconsejable incorporarlo a entornos de produccion.  
*"De los cobardes no hay nada escrito"*

- Hay que aprovechar que es una tecnologia gratuita (hasta que Bill Gates diga lo contrario)

# Alguna pregunta?

# Gracias.
