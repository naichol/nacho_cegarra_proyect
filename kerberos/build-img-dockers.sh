#!/bin/bash
##################VARIABLES#########################

IMAGEN="kerberos-img"
KDC="kdc"
KADMIN="kadmin"
REALM=".nacho.org"
LOCALDIR="/var/dockers-storage/"
SERVERIP=''
########### CREACION DE IMAGEN BASE ###############

docker build \
	--tag $IMAGEN .

########### PREPARAMOS EL SYSTEMA #################

# Paramos y removemos los containers si existen
docker stop $KDC $KADMIN temporal &>/dev/null
docker rm $KDC $KADMIN temporal &>/dev/null


# creamos el directorio local donde tendremos un backup 
# ATENCION!! en el caso de que exista el directorio es borrado
rm -rf $LOCALDIR/kerberos/krb5kdc &>/dev/null
mkdir -p $LOCALDIR/kerberos/krb5kdc &>/dev/null


# Preparamos la ejecucion de un container transaccional
# que nos servira para tomar la carpeta del kdc de kerberos 
# donde estara la informacion y la configuracion del KDC
# una vez conseguida la informacion lo cerramos y destruimos

docker run -d -t --name temporal $IMAGEN /bin/bash
docker cp temporal:/var/kerberos/krb5kdc $LOCALDIR/kerberos/
docker stop temporal
docker rm temporal


############## CREAMOS LOS CONTAINERS FINALES #####
# KADMIN container

docker create \
	--name $KADMIN \
	--expose 749 \
	--publish 749:749 \
	--hostname $KADMIN$REALM \
	--restart always \
	--volume /etc/localtime:/etc/localtime:ro \
	--volume $LOCALDIR/kerberos/krb5kdc:/var/kerberos/krb5kdc \
	--volume /var/kerberos/krb5kdc \
	--entrypoint kadmind \
	$IMAGEN \
	-nofork

# KDC container
docker create \
	--name $KDC \
	--expose 88 \
	--publish 88:88 \
	--link $KADMIN:$KADMIN$REALM \
	--hostname $KDC$REALM \
	--volume /etc/localtime:/etc/localtime:ro \
	--volumes-from $KADMIN \
	--restart always \
	--entrypoint krb5kdc \
	$IMAGEN \
	-n

####################################################
#se ejecuta por este orden, y en una linea
# docker start kadmin kdc
# docker stop kadmin kdc


