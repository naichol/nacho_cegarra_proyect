#!/bin/bash

# Archivo de startup que se ejecuta al crear la imagen del docker kdc

##### VARIABLES #####
admin='root/admin'
pass='pass'
realm='NACHO.ORG'

########################################################################
# Modificacion de archivos y creacion de base de datos
########################################################################

sed -i "s/EXAMPLE.COM/$realm/g" /var/kerberos/krb5kdc/kadm5.acl
sed -i "s/EXAMPLE.COM/$realm/g" /var/kerberos/krb5kdc/kdc.conf 
kdb5_util -P $pass -r $realm create -s 
kadmin.local -q "addprinc -pw $pass $admin"
