# Notas de instalacion de kerberos (KDC/KADMIN)  
## Requisitos previos a tener en cuenta:  
1.Disponer del paquete docker para tu ditribucion y que esta lo soporte.  
<pre>yum install docker</pre>  
2.Tener el servicio en ejecucion.  
<pre>systemctl start docker.service</pre>  
3.Que los scripts que acompañan esta documentacion han de tener permisos de ejecucion.  
<pre>chmod +x build-img-dockers.sh</pre>  
<pre>chmod +x startup.sh</pre>  
4.Si no se dispone de las imagenes de los sistemas es obligatorio tener conexion a internet.  
## Creacion de la imagen de kerberos  
Desde la carpeta donde esta el Dockerfile se configura el krb5-server.conf con los parametros del /etc/krb5.conf de los containers.  
Se edita las variables en el script de "build-img-dockers.sh", donde tendremos que especificar en que carpeta del sistema queremos guardar la informacion de forma persistente.  
Se edita tambien el startup.sh con los mismos parametros realcionados con los otros dos archivos anteriormente editados.  
## Comandos y parametros de la instalacion  
<pre>docker build --tag "kerberos-img" .</pre>    
Ejecutamos un container con el comando run para extraer la informacion que necesitaremos montar a nuestros dockers.  
<pre>docker run -d -t --name temporal $IMAGEN /bin/bash</pre>  
Donde el comando run crea el container y lo arranca, con -d lo deja en el background con la -t lo ejecuta como una tty, le especificamos la imagen y el comando a arrancar.  
En este caso el comando es un bash, pero es indiferente, solo se usa para poder extraer de el la carpeta /var/kerberos/krb5kdc que contiene los keytabs y la DB de kerberos que comparten los 2 servicios.    
Disponemos de toda la informacion en la carpeta del host /var/docker-storage/kerberos/krb5kdc  
## Container para el kadmin  
Primero es muy importante crea el kadmin, es el encargado de gestionar las cuentas, el kdc depende de el.  
Le montamos la carpeta /etc/localtime como solo lectura por que el contenedor no tiene la misma configuracion de hora que el host,y nos puede dar muchos problemas.  
<pre>docker create 
	--name $KADMIN 
	--expose 749 
	--publish 749:749 
	--hostname $KADMIN$REALM 
	--restart always 
	--volume /etc/localtime:/etc/localtime:ro 
	--volume $LOCALDIR/kerberos/krb5kdc:/var/kerberos/krb5kdc 
	--volume /var/kerberos/krb5kdc 
	--entrypoint kadmind 
	$IMAGEN 
	-nofork
</pre>
## Container para el kdc  
Muy similar al kadmin salvo que los puertos cambian y que para el kdc añadimos el parametro link que que lo que hace es una entrada en el /etc/hosts de container con la ruta al kadmin con la ip que dicho container tenga.  
Tambien se hereda la carpeta expuesta por el container KADMIN.  
<pre>docker create 
	--name $KDC 
	--expose 88 
	--publish 88:88 
	--link $KADMIN:$KADMIN$REALM 
	--hostname $KDC$REALM 
	--volume /etc/localtime:/etc/localtime:ro 
	--volumes-from $KADMIN 
	--restart always 
	--entrypoint krb5kdc 
	$IMAGEN 
	-n
</pre>
## FORMA DE USO  
Es muy importante a la hora de la ejecucion arrancar los containers por un orden concreto,primero el kadmin y luego el kdc.  
Si no se cumple el orden de arrancada puede courrir que el kdc reciba una peticion por parte de un host, el se intente comunicar con el kadmin , este no responde.  
Por lo tanto  
<pre>docker start kadmin kdc</pre>  
<pre>docker stop kdc kadmin</pre>  
Los contenedores estan creados con el parametro --restart always que quiere decir que siempre que el servicio de docker este funcionando se activaran ellos solos.  
Puede ser muy interesante activar el docker siempre en el sistema.  
<pre>systemctl enable docker.service</pre>  
De esta manera se automatizan los servicios con el arranque del sistema.  
## Configuraciones adicionales  
Los archivos de log son gestionados por STDERR para poder ser monitorizados atraves de comando.  
<pre>docker logs kadmin</pre>  
<pre>docker logs kdc</pre>  
Si quisieramos reinstalar los dockers y mantener las cuentas, es tan facil como renombrar la carpeta /var/docker-storage/kerberos/krb5kdc a krb5kdc.bak , ejecutar el instalador copiar todo el contenido de la carpeta vieja dentro de la nueva , (con los dockers apagados) y al levantarlos ya estarian listos para hacer un start con la configuracion vieja.  

Nacho Cegarra Cayetano <naicho@gmail.com>