# Presentacion del proyecto 
## Objetivo del proyecto  
Intentar simular un servidor empresarial usando dockers para los servicios 
en containers individuales.    
## Se puede dockerizar todo?  
Hay que tener claro que los dockers son una gran ventaja en cuanto a la 
usabilidad. Pero tambien hay que ser conscientes en que no todo se puede 
dockerizar.  
No tenemos systemctl dentro del container asi que hay que apañarselas para
ver como son los script de arranque,y levantar todos los servicios 
dependientes.
Los servicios que dependen de las llamadas al nucleo no se deben de 
implementear via docker, por que la unica manera de implementarlo es 
dando al container privilegios.  
## Que eso de privilegios?  
Pues consiste basicamente en montar la carpeta /proc  del host en el 
container.  
Eso quiere decir que el container tiene "autoridad" para alterar el 
funcionamiento del host.En otras palabras, si nos hackean el container 
podrian incluso formatearnos el disco duro.Por lo tanto se descarta la 
opcion de dar privilegios.
## Hay mas limitaciones?
Si la plataforma docker no funciona sobre hosts de 32 bits.
Dentro del container no se dispone de systemctl, por lo que hace complicado 
la implementacion de servicios complicados que requieren de muchas dependencias.
Si no tenemos cuidado podremos saturar el almacenamiento del disco, porque
cada imagen ocupa unos 250MB sin apps, en cuanto la empiezas a llenar
crece enseguida y el propio sistema de docker va haciendo snapshots del
sistema que consisten en sacar una instantanea del momento actual de la
imagen y almacenarla.
## Vamos a ver las ventajas
Los dockers son muy interesantes para poder disponer de aplicaciones 
complejas y ser ejecutadas en diferentes entornos, se podria por ejemplo 
tener un python 2.6 en un ubuntu y un python 3 en centos a la escucha de
su entrada estandar, lo unico que haces es redireccionar el programa al docker
y podrias ver el comportamiento de tu aplicacion en diferentes plataformas.
Para los sysadmin,la posibilidad de probar configuraciones de red de grandes 
estructuras, por ejemplo hacer pruebas con la configuracion LDAP creando 
todo el arbol de directorios en menos de 1 minuto,y un largo etc.
## Que necesitamos?  
Un ordenador 64bits , descargarnos el paquete de docker "docker" o "docker.io"
Con el servicio arrancado podremos:  
1. Crear imagenes (el propio paquete incluye el repositorio github de docker)  
2. Crear/borrar/copiar/modificar containers.  
3. Revisar logs de los serviciosdockerizados.  
4. Hacer backups/exportaciones/importaciones de dockers para llevar a otra maquina. 
  

Es muy recomendable ir a la web [docker.com](http://docker.com) hay esta
toda la informacion relacionada con esta tecnologia.
## Los ejemplos que yo os muestro son los siguientes
## Kerberos
Creacion de 2 containers uno kadmin y otro kdc, partiendo de archivos de configuracion.  
## LDAP-server  
Creacion de el servicio de ldap con una base de datos con ejemplos para confirmar que se puede implementar con exito.Ojo no es LDAP v3 , para poder implementar esa version haria falta una capa mas de SSL y mas puertos.  
## SAMBA  
Creacion de recursos en la red, partiendo del smb.conf, en mi caso implemente  
una carpeta compartida en la red con acceso desde el servidor.  

### Autor : Nacho Cegarra Cayetano <naichol@gmail.com>